﻿using System;
using System.Diagnostics;
using System.IO;

namespace qBitTor
{
    public class QbittorWin
    {
        private readonly string pathToTor = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Tor", @"tor.exe");
        private readonly string pathToQbittorrent = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"qBittorrentPortable.exe");

        public void RunTor()
        {
            try
            {
                using (Process torProcess = new Process())
                {
                    torProcess.StartInfo.FileName = pathToTor;
                    torProcess.StartInfo.CreateNoWindow = true;
                    torProcess.Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Program tor.exe not found.", e);
            }
        }

        public void RunQbittorrent()
        {
            try
            {
                using (Process qbittorProcess = new Process())
                {
                    qbittorProcess.StartInfo.FileName = pathToQbittorrent;
                    qbittorProcess.Start();
                    qbittorProcess.WaitForExit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Program qbittorrent.exe not found", e);
            }
        }

        public void KillProcess()
        {
            try
            {
                foreach (Process proc in Process.GetProcessesByName("tor"))
                {
                    proc.Kill();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Process tor.exe not found.", e);
            }
        }
    }
}