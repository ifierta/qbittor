﻿using System;
using System.Diagnostics;
using System.IO;

namespace qBitTor
{
    public class QbittorMac
    {
        private readonly string pathToTor = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tor", @"tor");
        private readonly string pathToQbittorrent = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "qBittorrent.app", "Contents", "MacOS", @"qbittorrent");

        public void RunTor()
        {
            try
            {
                using (Process torProcess = new Process())
                {
                    torProcess.StartInfo.FileName = pathToTor;
                    torProcess.StartInfo.CreateNoWindow = true;
                    torProcess.Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Program tor.real not found.", e);
            }
        }

        public void RunQbittorrent()
        {
            try
            {
                using (Process qbittorProcess = new Process())
                {
                    qbittorProcess.StartInfo.FileName = pathToQbittorrent;
                    qbittorProcess.Start();
                    qbittorProcess.WaitForExit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Program qbittorrent not found", e);
            }
        }

        public void KillProcess()
        {
            try
            {
                foreach (Process proc in Process.GetProcessesByName("tor.real"))
                {
                    proc.Kill();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Process tor.real not found.", e);
            }
        }
    }
}