﻿using System;

namespace qBitTor
{
    public static class Programm
    {

        public static void Main()
        {
            
            string currentOS = Convert.ToString(OSInfo.GetOperatingSystem());
            
            if(currentOS == "WINDOWS")
            {
                QbittorWin winProcess = new QbittorWin();
                winProcess.RunTor();
                winProcess.RunQbittorrent();
                winProcess.KillProcess();
            }
            else
            {
                QbittorMac macProcess = new QbittorMac();
                macProcess.RunTor();
                macProcess.RunQbittorrent();
                macProcess.KillProcess();
            }
            
        }
    }
}
